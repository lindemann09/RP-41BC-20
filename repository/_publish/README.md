# Instruction for lecturer of R courses

How to create new a code repository?

1. Make Gitlab repository
2. Create a `public` and `repository` folder
3. Copy this `_publish` folder into `repository`
4. For continuous integration, copy the `.gitlab-ci.yml` to base directory of the Gitlab repository
5. Adapt .user, .git_repository ect. in `update.R`
6. (optional) add your folders and files to the repository
7. (optional website) add html files to public
8. git commit all changes and push.


Gitlab will publish all files in `public` via `https://username.gitlab.io/gitlab-repository`.

**Gitlab CI** (`.gitlab-ci.yml`): With each commit, a copy of `update.R` and a zip of the current repository folder (`repository.zip`) will added to `public`.

# Instructions for students

To get started and to initialize the repository on the local PC, students  have to run:

``
source("https://username.gitlab.io/gitlab-repository/update.R")
``

Later changes of the repository will become available after calling the local `update.R` file.

Please note that files in students local repositories will not be overwritten to keep individual changes and notes. Instruct students to delete files/folder whenever a file updates is required.

---

*Released under the MIT License*
(c) Oliver Lindemann <lindemann@cognitive-psychology.eu>
